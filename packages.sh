#!/bin/bash

#  ______          _                           _           _        _ _ 
#  | ___ \        | |                         (_)         | |      | | |
#  | |_/ /_ _  ___| | ____ _  __ _  ___  ___   _ _ __  ___| |_ __ _| | |
#  |  __/ _` |/ __| |/ / _` |/ _` |/ _ \/ __| | | '_ \/ __| __/ _` | | |
#  | | | (_| | (__|   < (_| | (_| |  __/\__ \ | | | | \__ \ || (_| | | |
#  \_|  \__,_|\___|_|\_\__,_|\__, |\___||___/ |_|_| |_|___/\__\__,_|_|_|
#                             __/ |                                     
#                            |___/                                      
#                 _       _   
#                (_)     | |  
#   ___  ___ _ __ _ _ __ | |_ 
#  / __|/ __| '__| | '_ \| __|
#  \__ \ (__| |  | | |_) | |_ 
#  |___/\___|_|  |_| .__/ \__|
#                  | |        
#                  |_|        
#  


if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


Packages=(    

    # Browsers
    Firefox
    brave-browser
    google-chrome-stable
    
    # Multimedia
    vlc
    gthumb
    
    # Themes and Icons
    flat-remix-gtk
    
    
    # Terminal related
    youtube-dl
    zsh
    htop
    neofetch
    vim
    lolcat
    alacritty
    
    # Fonts
    fonts-font-awesome
    fonts-firacode
    
    # Code Editors
    code
    sublime-text
    
)


for PKG in "${Packages[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo apt install -y "$PKG"
done

echo
echo "Done!"
echo

