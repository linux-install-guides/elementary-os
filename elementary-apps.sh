#!/bin/bash

#   _____ _                           _                      ___                  
#  |  ___| |                         | |                    / _ \                 
#  | |__ | | ___ _ __ ___   ___ _ __ | |_ __ _ _ __ _   _  / /_\ \_ __  _ __  ___ 
#  |  __|| |/ _ \ '_ ` _ \ / _ \ '_ \| __/ _` | '__| | | | |  _  | '_ \| '_ \/ __|
#  | |___| |  __/ | | | | |  __/ | | | || (_| | |  | |_| | | | | | |_) | |_) \__ \
#  \____/|_|\___|_| |_| |_|\___|_| |_|\__\__,_|_|   \__, | \_| |_/ .__/| .__/|___/
#                                                    __/ |       | |   | |        
#                                                   |___/        |_|   |_|        
#  

Packages=(
    
    # Comgen - Comment Generator: generate styled comments for your code.
    # https://appcenter.elementary.io/com.github.jeremyvaartjes.comgen/
    com.github.jeremyvaartjes.comgen

    # DotFonts - Find beautiful fonts and easy install on elementaryOS
    # https://github.com/aimproxy/dotfonts
    com.github.aimproxy.dotfonts

    # Fondo - Find the most beautiful wallpapers for your desktop.
    # https://github.com/calo001/fondo
    com.github.calo001.fondo

    # Minder - Mind-mapping application
    # https://github.com/phase1geo/minder/
    com.github.phase1geo.minder

    # Norka - Text Editor
    # https://github.com/TenderOwl/Norka
    com.github.tenderowl.norka

    # Planner - Task manager with Todoist support designed for GNU/Linux
    # https://github.com/alainm23/planner
    com.github.alainm23.planner

    # Quilter - Markdown and text Editor (similar to Norka)
    # https://github.com/lainsce/quilter/
    com.github.lainsce.quilter

    # Rakugaki - Casual drawing and doodling app
    # https://github.com/lainsce/rakugaki/
    com.github.lainsce.rakugaki

    # Screencast - Simple Screen Recorder
    # https://github.com/artemanufrij/screencast
    com.github.artemanufrij.screencast

    # Regex Tester - Regex testing app
    # https://github.com/artemanufrij/regextester
    com.github.artemanufrij.regextester

    # Webpin - Create application menu entry for any web page(app)
    # https://github.com/artemanufrij/webpin
    com.github.artemanufrij.webpin

    # Ciano - multimedia file converter
    # https://github.com/robertsanseries/ciano
    com.github.robertsanseries.ciano

    # Color Picker - simple color picker
    # https://github.com/RonnyDo/ColorPicker
    com.github.ronnydo.colorpicker

    # Resizer - Simple Image resizer
    com.github.peteruithoven.resizer

    # Wallpaperize - Beautify your oddly-sized wallpapers
    com.github.philip-scott.wallpaperize

    # Notes Up - Markdown notes manager
    com.github.philip-scott.notes-up
    
)


if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo
echo "Starting to install Elementary OS Apps: "

for PKG in "${Packages[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo apt install -y "$PKG"
done

echo
echo "Done!"
echo

