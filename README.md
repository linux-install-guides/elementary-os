# Elementary OS Post Installation
Elementary OS comes with a beautiful desktop with limited configurable settings. In this repo, I try to configure it to my taste and install my prefered applications.

<br>
<hr>
<br>

### Third Party Repos

| Repo | Description | Source |
| - | - | - |
| Pantheon Tweaks | This repo provides with pantheon tweaks which can be used to make changes to Pantheon DE like themes, icons, sounds etc.. | [Pantheon-Tweaks](https://github.com/pantheon-tweaks/pantheon-tweaks) |
| Brave Browser | Privacy focused browser based on chromium | [Brave-browser](https://brave.com/linux/) |
| Google Chrome | Google chrome browser | None |
| Visual Studio Code | Code Editor | [VS-Code](https://code.visualstudio.com/docs/setup/linux) |
| Sublime Text | Code Editor | [Sublime-Text](https://www.sublimetext.com/docs/3/linux_repositories.html#apt) |

<br>
<hr>
<br>

### Packages

Packages I regularly use are installed using [packages.sh](packages.sh) script. While apps that I install from Application center(Elementary specific) are installed through [elementary-apps.sh](elementary-apps.sh) script. I prefer to install these apps using Application center because it has many good and new interesting apps. Also figuring out their package names is impossible without using `apt search` command.

### Pywal color scheme

I also use [Pywal](https://github.com/dylanaraps/pywal) to set colors on my desktop. 

## Desktop screenshots

* Desktop

![Desktop Background](Pics/desktop.png)

* Pywal Colorscheme and Icon 
![Pywal](Pics/colorscheme.png)

* VS-Code colorscheme
![vscode](Pics/vscode.png)
