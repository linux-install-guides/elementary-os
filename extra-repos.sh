#!/bin/bash

#   _____         _  ______          _          ______                 
#  |____ |       | | | ___ \        | |         | ___ \                
#      / /_ __ __| | | |_/ /_ _ _ __| |_ _   _  | |_/ /___ _ __   ___  
#      \ \ '__/ _` | |  __/ _` | '__| __| | | | |    // _ \ '_ \ / _ \ 
#  .___/ / | | (_| | | | | (_| | |  | |_| |_| | | |\ \  __/ |_) | (_) |
#  \____/|_|  \__,_| \_|  \__,_|_|   \__|\__, | \_| \_\___| .__/ \___/ 
#                                         __/ |           | |          
#                                        |___/            |_|          
#   _____          _        _ _   _____           _       _   
#  |_   _|        | |      | | | /  ___|         (_)     | |  
#    | | _ __  ___| |_ __ _| | | \ `--.  ___ _ __ _ _ __ | |_ 
#    | || '_ \/ __| __/ _` | | |  `--. \/ __| '__| | '_ \| __|
#   _| || | | \__ \ || (_| | | | /\__/ / (__| |  | | |_) | |_ 
#   \___/_| |_|___/\__\__,_|_|_| \____/ \___|_|  |_| .__/ \__|
#                                                  | |        
#                                                  |_|        
#  



# Check "sudo" access
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# These packages are needed to add any third-party repos
apt install -y software-properties-common apt-transport-https curl gnupg


# Pantheon Tweaks - Tweak settings for Pantheon environment
# https://github.com/pantheon-tweaks/pantheon-tweaks
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv c42d52715a84c7d0d02fc740c1d89326b1c71ab9
echo -e "deb http://ppa.launchpad.net/philip.scott/pantheon-tweaks/ubuntu focal main\ndeb-src http://ppa.launchpad.net/philip.scott/pantheon-tweaks/ubuntu focal main" | sudo tee /etc/apt/sources.list.d/pantheon-tweaks.list
sudo apt update -y
sudo apt install pantheon-tweaks -y


# Brave Browser
# https://brave.com/linux/
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update -y


# Google Chrome
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'


# VS Code
# https://code.visualstudio.com/docs/setup/linux
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'


# Sublime Text
# https://www.sublimetext.com/docs/3/linux_repositories.html#apt
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list


# Alacritty Terminal
sudo add-apt-repository ppa:aslatter/ppa


# Flat Remix GTK Theme
sudo add-apt-repository ppa:daniruiz/flat-remix
sudo apt update -y
